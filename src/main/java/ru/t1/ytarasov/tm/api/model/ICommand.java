package ru.t1.ytarasov.tm.api.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.exception.AbstractException;

public interface ICommand {

    void execute() throws AbstractException;

    @Nullable
    String getName();

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

}
