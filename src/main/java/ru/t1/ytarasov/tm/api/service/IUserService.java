package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password) throws AbstractException;

    @NotNull
    User create(@Nullable String login, @Nullable String password,@Nullable  String email) throws AbstractException;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role) throws AbstractException;

    @Nullable
    User findByLogin(@Nullable String login) throws AbstractException;

    @Nullable
    User findByEmail(@Nullable String email) throws AbstractException;

    @Nullable
    User removeByLogin(@Nullable String login) throws AbstractException;

    @Nullable
    User removeByEmail(@Nullable String email) throws AbstractException;

    @NotNull
    User setPassword (@Nullable String id, @Nullable String password) throws AbstractException;

    @NotNull
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName) throws AbstractException;

    boolean isLoginExist(@Nullable String login) throws AbstractException;

    boolean isEmailExists(@Nullable String email) throws AbstractException;

    void lockUser(@Nullable String login) throws AbstractException;

    void unlockUser(@Nullable String login) throws AbstractException;

}
