package ru.t1.ytarasov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusById extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-change-by-id";

    @NotNull
    public static final String DESCRIPTION = "Change status of project by id";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CHANGE STATUS OF THE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @Nullable final String userId = getAuthService().getUserId();
        getProjectService().changeProjectStatusById(userId, id, status);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
